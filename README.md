# CoronaHelp
Sebuah website yang hadir untuk menghubungkan donatur dan rumah sakit/klinik di masa-masa krisis seperti sekarang.

## Desain Web
* Lihat rancangan desain web [disini](https://www.figma.com/file/75LQBRtsMc0q1PCc1JvleA/CoronaHelp?node-id=0%3A1)
* Lihat web yang sudah di deploy [disini](https://corona-dingdong.herokuapp.com)

