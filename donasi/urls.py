from django.urls import path, include
from . import views

app_name = 'donasi'

urlpatterns = [
    path('', views.donasi, name = "donasi"),
    path('<name>/', views.detail, name = "detail"),
]