$(document).ready(function() {
    var data = [
        {
            "gambar": "/static/img/rs-temp.svg",
            "nama": "RS Pelni",
            "alamat": "Jakarta",
            "kategori": ["Surgical Cup", "Gown Bedah Disposable", "Hand Gloves"],
            "butuh": "Gown Bedah Disposable",
            "link" : "nanti pake dispatcher"
        },
        {
            "gambar": "/static/img/rs-temp.svg",
            "nama": "Klinik Makara",
            "alamat": "UI",
            "kategori": ["Google", "Suntik"],
            "butuh": "masker",
            "link" : "nanti pake dispatcher"
        },
        {
            "gambar": "/static/img/rs-temp.svg",
            "nama": "RS Sehat",
            "alamat": "Bogor",
            "kategori": ["Sepatu boot panjang", "Hand sanitizer", "APD"],
            "butuh": "APD",
            "link" : "nanti pake dispatcher"
        },
        {
            "gambar": "/static/img/rs-temp.svg",
            "nama": "RS Lorem Ipsum Dolor Sit Amet",
            "alamat": "Mars",
            "kategori": ["Masker (masker N-95 dan bedah)", "Face shield", "Makanan"],
            "butuh": "Makanan",
            "link" : "nanti pake dispatcher"
        },
    ];

    console.log(data[0].kategori)
    console.log(data[0].kategori.length)
    console.log(data[0].kategori[0])

    for(i = 0; i < data.length; i++) {
        $("#list").append(
            "<div class='unit'>" +
            "<div class='foto'>" + "<img class='foto-rs' src=" + data[i].gambar + " alt=\"\">" +
            "</div>" +
            "<div class='detail'>" +
            "<h3>" + data[i].nama + "</h3>" +
            "<h5>" + data[i].alamat + "</h5>" +
            "<div class='list-kategori' id='lk" + i +"'></div>" +
            "<p>Butuh Segera: " + data[i].butuh + "</p>" +
            "<a href='corona-dingdong/donasi/" + data[i].link + "'>Donasi</a>" +
            "</div>" +
            "</div>"
        );

        for(j = 0; j < data[i].kategori.length; j++) {
            $('#lk' + i).append(
                "<div class='kategori'>" + data[i].kategori[j] + "</div>"
            );
        }
    }
});