from django.urls import path, include
from . import views

app_name = 'donasibarang'

urlpatterns = [
    path('', views.donasibarang, name = "donasibarang"),
    path('<name>/', views.detailbarang, name = "detailbarang"),
]